import {Mongo} from 'meteor/mongo';

export const LostDataAfterPhotoProblem = new Mongo.Collection('lostdataafterphotoproblem');
