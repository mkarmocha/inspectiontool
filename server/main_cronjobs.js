import fs from 'fs';
let dbincadea = [];
// // todo set isuzu db name
// //dbincadea["isuzu"] = "[Hyundai Motors New Zealand Ltd$";
dbincadea["hyundai"] = "[Hyundai Motors New Zealand Ltd"; // properties.get('hyundai.incadeaDatabase'); // Meteor.settings.incadeaDatabaseHyundai;// "[Hyundai Motors New Zealand Ltd";

var getIncadeaSelectNewVehicleDataStr = function (company, rowversionStr) {

    // let rowversionStr = "1379990472";
    var selstr =
        "SELECT "
        + " convert(bigint, " + dbincadea[company] + "$Vehicle].[timestamp]) AS 'rowversion',"
        + " " + dbincadea[company] + "$Vehicle].[VIN] AS 'vin',"
        + " " + dbincadea[company] + "$Vehicle].[License No_]  AS 'rego',"
        + " " + dbincadea[company] + "$Vehicle].[Purchase Invoice Date],"
        + " " + dbincadea[company] + "$Vehicle].[Purchase Receipt Date],"
        + " " + dbincadea[company] + "$Vehicle].[Creation Date],"
        + " " + dbincadea[company] + "$Vehicle].Model AS 'modelName',"
        + " " + dbincadea[company] + "$Vehicle Option].[Description] as colour,"
        + " ' ' AS 'KeyNo',"
        + " " + dbincadea[company] + "$Vehicle].[Dealer Request Code] as 'Dealer Request Code',"
        + " " + dbincadea[company] + "$Customer].[Name] as 'Incadea Dealer Name',"
        + " " + dbincadea[company] + "$Customer].[No_] as 'Incadea Dealer No',"
        + " " + dbincadea[company] + "$Customer].[City] as 'Incadea City',"
        + " 'HYUNDAI' as 'make',"
        + " GETDATE() as 'Updated by Cron Job AT'"
        + " FROM " + dbincadea[company] + "$Vehicle] "
        + " JOIN " + dbincadea[company] + "$Vehicle Option] "
        + " ON " + dbincadea[company] + "$Vehicle Option].[VIN] = " + dbincadea[company] + "$Vehicle].[VIN]"
        + " AND " + dbincadea[company] + "$Vehicle Option].[Option Type] = '1'"

        + " JOIN " + dbincadea[company] + "$Dealer Customer] "
        + " ON " + dbincadea[company] + "$Dealer Customer].[Dealer Code] = " + dbincadea[company] + "$Vehicle].[Dealer Request Code]"
        + " JOIN " + dbincadea[company] + "$Customer] "
        + " ON " + dbincadea[company] + "$Customer].[No_] = " + dbincadea[company] + "$Dealer Customer].[Customer No_]"
        + " WHERE " + dbincadea[company] + "$Vehicle].[timestamp] > CONVERT(ROWVERSION, " + rowversionStr + ") "
        + " ORDER BY " + dbincadea[company] + "$Vehicle].[timestamp]  ASC ";

    console.log("SELECTSTR : " + selstr);
    return selstr;
};

if (Meteor.isServer) {

    SyncedCron.start();

    SyncedCron.add({
        name: 'Get Hyundai new Vehicles Data for Inspection ',
        schedule: function (parser) {
            // parser is a later.parse object

             return parser.recur().on(8,9,10,11,12,13,14,15,16,23).hour();
            //return parser.recur().on('12:17:00').time();
            // return parser.text("every 30 mins");
        },
        job: function () {

            // get new row updateing

            let rowVersion = "0";
            let nextRowVersion = "0";

            var text = fs.readFile(process.env["PWD"] + "/../rowversion.txt", 'utf8', (err, data) => {
                if (err) throw err;
                console.log(JSON.stringify(data));
                rowVersion = data;
                console.log("START rowversion " + rowVersion);
            });

            let data = [];
            Sql.connection = new Sql.driver.Connection({
                "server": "10.92.40.7",  // .26
                "database": "HNZNAV1", // HNZNAVDEV
                "user": "webhaven",
                "password": "w3bhav3n",
                "options": {
                    "useUTC": false,
                    "appName": "MeteorApp"
                },
                "connectionTimeout": 300000,
                "requestTimeout": 300000,
                "pool": {
                    "idleTimeoutMillis": 300000,
                    "max": 100
                }
            }, Meteor.bindEnvironment(function (err) {
                if (err) {
                    console.log("1. Can't connect to Incadea db " + JSON.stringify(err));
                    // console.log("Test. Sql.q ");
                } else {
                    var selstr = getIncadeaSelectNewVehicleDataStr("hyundai", rowVersion);
                    try {
                        data = Sql.q(selstr);

                        if (data && data.length > 0) {
                            console.log("new vehicle data / num " + JSON.stringify(data.length));
                            console.log("new vehicle data / first " + JSON.stringify(data[0]));

                            let numAdded = 0;
                            data.forEach(function(added) {
                                Meteor.call('vehiclesadded.update', added);
                                numAdded++;
                                nextRowVersion = added.rowversion;
                            });

			    // nextRowVersion = data[100].rowversion;
                            console.log("next rowversion 1 " + nextRowVersion + " num added " + numAdded);
                            fs.writeFile(process.env["PWD"] + "/../rowversion.txt", nextRowVersion, 'utf8', (err) => {
                                if (err) throw err;
                            });
                            let uploadUser = { "id": "uploadid",  "username": "upload",
                            "role":"Admin System", "pdiApproved": true, "company": "ALL", "dealer": "ALL"}
                            Meteor.call('uploading.uploadAndUpdateNewVehicles', ["PDI", "H100", "Body", "LS"],
                                uploadUser.id, uploadUser.username,
                                uploadUser.role, uploadUser.pdiApproved,
                                uploadUser.company, uploadUser.dealer);
                        }

                    } catch (e) {
                        console.log("Err in Select `Vehicle New Vehicle Data' to MSServer" + e);
                    }
                }

                console.log("next rowversion 2 " + nextRowVersion);

            }));
        }

    });


}
